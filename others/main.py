from envs.flatland import Flatland
from mind.nn import NN
from mind.evolution import Evolution
import itertools
import random
import sys
import time

class Main:
    def __init__(self):
        self.agents_amount = 8
        self.species_amount = 1
        self.running_times = 50
        self.agents = [NN() for _ in range(self.agents_amount)]
        self.env = Flatland(self.agents, self.species_amount)
        self.evolution = Evolution()
        self.restore()

    def run(self):
        for j in itertools.count():
            try:
                self.env.generation_start(self.agents, j)

                for i in range(self.running_times):
                    for agent in self.agents:
                        inpt = self.env.get_input(agent)
                        output = agent.input(inpt)
                        self.env.set_output(agent, output)
                    self.env.iteration_end(i)

                self.env.generation_end(self.agents, j)

                agents_by_specie = {i: self.env.agents_by_specie(i) for i in range(self.species_amount)}
                agents, species = self.evolution.generation_end(agents_by_specie)
                self.agents = agents
                self.env.species = species

            except KeyboardInterrupt:
                key = input()
                if key == 'd': self.env.drawing_enabled = True
                elif key == 'dd': self.env.drawing_enabled = False
                elif key == 's': self.save()
                elif key == 'i': self.interact()

    def save(self):
        for i, agent in enumerate(self.agents): agent.save(i)
        print ('saved')

    def restore(self):
        for i, agent in enumerate(self.agents): agent.restore(i)
        print ('restored')

    def interact(self):
        x = None #WIP
        print (self.agents[0].input(input()))


Main().run()
