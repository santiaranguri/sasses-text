import pygame
import random
import numpy as np
import time

WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)

class Flatland:
    def __init__(self, agents, species_amount):
        self.ids = {agent: i for i, agent in enumerate(agents)}
        self.species = {agent: i % species_amount for i, agent in enumerate(agents)}
        self.species_amount = species_amount
        self.food_pos = {agent: [random.uniform(.2, .8) for _ in range(2)] for agent in agents}
        self.min_dist = 0.005
        self.velocity = .2
        self.drawer = Drawer(self)
        self.avg_acc = [0] * species_amount
        self.drawing_enabled = False

    def reset_params(self, agents):
        self.agents = agents
        #self.agent_pos = {agent: [random.uniform(0, 1) for _ in range(2)] for agent in agents}
        self.agent_pos = {agent: [random.uniform(0, 1) for _ in range(2)] for agent in agents}
        self.fitnesses = {agent: 0 for agent in agents}
        self.food_pos = {agent: [random.uniform(.2, .8) for _ in range(2)] for agent in agents}

    def agents_by_specie(self, specie):
        return {agent: fitnessfor agent, fitnessin self.fitnesses.items() if self.species[agent] == specie}

    def get_input(self, agent):
        #inpt = [[pos[0], pos[1], specie] for pos, specie in zip(self.agent_pos.values(), self.species.values())]
        #return np.array(inpt).flatten()
        return self.agent_pos[agent] + self.food_pos[agent]
        '''
        min_distance = 100
        best_agent = None
        for other_agent in self.agents:
            distance = sum([abs(x1 - x2) for x1, x2 in zip(self.agent_pos[agent], self.agent_pos[other_agent])])
            if agent != other_agent and distance < min_distance:
                min_distance = distance
                best_agent = other_agent
        #if self.ids[agent] == 0: print (self.agent_pos[agent] + self.agent_pos[best_agent])
        return [x1 - x2 for x1, x2 in zip(self.agent_pos[agent], self.agent_pos[best_agent])]
        '''

    def set_output(self, agent, output):
        for i, (pos, delta) in enumerate(zip(self.agent_pos[agent], output)):
            delta = (delta - .5) * self.velocity
            if   1 < pos + delta: self.agent_pos[agent][i] = 1
            elif 0 > pos + delta: self.agent_pos[agent][i] = 0
            else:                 self.agent_pos[agent][i] = pos + delta

    def generation_start(self, agents, i):
        self.reset_params(agents)

    def generation_end(self, agents, i):
        print ('Generation {}.'.format(i), end=' ')
        for specie in range(self.species_amount):
            best_fitnesses = sorted(self.agents_by_specie(specie).values())[int(len(self.agents_by_specie(specie)) / 2):]
            avg = np.average([v for v in best_fitnesses])
            self.avg_acc[specie] += avg
            print_avg = self.avg_acc[specie] / (i + 1)
            print ('Specie {}: Acc avg {}. Avg {}.'.format(specie, print_avg, avg))
        self.drawing_enabled = False

    def iteration_end(self, i):
        if self.drawing_enabled: self.drawer.draw()

        for agent in self.agents:
            self.food_pos[agent] = [dim + random.uniform(-.07, .07) for dim in self.food_pos[agent]]

        for agent in self.agents:
            if all([abs(x1 - x2) < .05 for x1, x2 in zip(self.food_pos[agent], self.agent_pos[agent])]):
                self.fitnesses[agent] += 1
                self.food_pos[agent] = [random.uniform(.2, .8) for _ in range(2)]

        '''
        if (i + 1) % 300 == 0:
            for agent in self.agents:
                self.fitnesses[agent] += 1 if self.species[agent] == 1 else 0
        for agent in self.agents:
            for other_agent in self.agents:
                #if self.species[agent] == 0 and self.species[other_agent] == 1:
                pos1, pos2 = self.agent_pos[agent], self.agent_pos[other_agent]
                if pos1 != pos2:
                    if all([abs(x1 - x2) < self.min_dist for x1, x2 in zip(pos1, pos2)]):
                        self.fitnesses[agent] += 1
                        self.fitnesses[other_agent] += 1
                        self.agent_pos[agent] = [random.uniform(0, 1) for _ in range(2)]
                        self.agent_pos[other_agent] = [random.uniform(0, 1) for _ in range(2)]
        '''

class Drawer:
    def __init__(self, flatland):
        self.fl = flatland
        self.width = 1200
        self.height = 700
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.font.init()
        self.font = pygame.font.SysFont('Arial', 12)

    def draw(self):
        self.screen.fill(BLACK)
        first_time = True
        for pos, specie, fitness, id in zip(self.fl.agent_pos.values(), self.fl.species.values(), self.fl.fitnesses.values(), self.fl.ids.values()):
            if first_time:
                first_time = False
                color = GREEN if specie == 1 else RED
                pos = [int(dim * screen * 0.95) + 15 for dim, screen in zip(pos, [self.width, self.height])]
                pygame.draw.circle(self.screen, color, pos, 5)
                text_surface = self.font.render('{} {}'.format(id, fitness), False, (WHITE))
                self.screen.blit(text_surface, (pos[0] - 5, pos[1] - 5))

        first_time = True
        for pos in self.fl.food_pos.values():
            if first_time:
                first_time = False
                new_pos = [int(dim * screen * 0.95) + 15 for dim, screen in zip(pos, [self.width, self.height])]
                pygame.draw.circle(self.screen, BLUE, new_pos, 5)
                pygame.display.flip()

        time.sleep(.5)
