import tensorflow as tf

vector = [tf.Variable([1]) for _ in range(2)]
indices = tf.constant([0])
updates = tf.constant([10])
result = tf.scatter_nd_add(vector, indices, updates)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    print ('result', sess.run(result))
