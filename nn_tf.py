import numpy as np
import tensorflow as tf

class NN:
    def __init__(self):
        self.sess = None #It's later assigned by env.py
        self.size = [8, 8, 8, 8, 2]

    def define_vars(self):
        self.input = tf.placeholder(tf.float32, [None, self.size[0]])
        weights = [tf.Variable(tf.random_normal([m, n])) for m, n in zip(self.size, self.size[1:])]
        biases = [tf.Variable(tf.random_normal([m])) for m in self.size[1:]]

        x = self.input
        for w, b in zip(weights, biases):
            x = tf.add(tf.matmul(x, w), b)
            x = tf.nn.relu(x)

        self.output = x

    def run(self):
        with tf.Session() as self.sess:
            self.define_vars()
            self.sess.run(tf.global_variables_initializer())
            self.predict()

    def predict(self):
        print (self.sess.run(self.output, {self.input: np.random.uniform(0, 1, (1, 8))}))

NN().run()
