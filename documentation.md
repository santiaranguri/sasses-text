#Envs
##Flat_env
###Levels
1) big food
2) food
3) shared food
4) borders (agents lose points if they go outside flatland)
5) region that gives points if the agent is there
6) region that gives points if both agents are there
7) collisions (if there was a collision and one agent went outside flatland, the other agent earns points)
8) guns for 'tiro al blanco' [not implemented yet]
9) guns for sumo (similar to collisions) [not implemented yet]
