import random
import numpy as np
from bidict import bidict
import sys
sys.path.append('../')
from env import Env
from flat_drawer import FlatDrawer

class FlatEnv(Env):
    velocity = .25
    food_dist = .1
    collision_dist = .02

    def __init__(self):
        super(FlatEnv, self).__init__()
        self.drawer = FlatDrawer(self)
        self.food_pos = {}
        self.relations = bidict({})
        self.level = 1

    def get_input(self, agent):
        #return agent.pos + self.get_enemy(agent).pos + self.food_pos[agent]
        return agent.pos + [0, 0] + self.food_pos[agent]

    def set_output(self, agent, output):
        agent.output = output

    def get_enemy(self, agent):
        if agent in self.relations: return self.relations[agent]
        elif agent in self.relations.inv: return self.relations.inv[agent]

    def iteration_end(self, i):
        level = self.level
        for agent in self.agents:
            pos1 = agent.pos
            multiplier = 3 if level == 1 else 1

            if level >= 3:
                other_agent = self.get_enemy(agent)
                pos2 = other_agent.pos

            #food
            if all([abs(x1 - x2) < self.food_dist * multiplier for x1, x2 in zip(pos1, self.food_pos[agent])]):
                agent.fitness += 1
                self.food_pos[agent] = [random.uniform(0, 1) for _ in range(2)]
                if level >= 3: self.food_pos[other_agent] = self.food_pos[agent]

            #movement
            for i, (pos, delta) in enumerate(zip(agent.pos, agent.output)):
                agent.prev_pos[i] = agent.pos[i]
                delta = (delta - .5) * 2 * self.velocity
                if   pos + delta > 1:   agent.pos[i] = 1
                elif pos + delta < 0:   agent.pos[i] = 0
                else:                   agent.pos[i] = pos + delta

                #borders
                if level >= 4 and not (0 < pos + delta < 1):
                    agent.fitness -= 5
                    if level >= 7 and other_agent.collision:
                        other_agent.fitness += 5

            #individual region
            if level >= 5 and all([0.1 < x < 0.3 for x in pos1]) and all([0.1 < x < 0.1 for x in pos2]):
                agent.fitness += .2

            #collaborative region
            if level >= 6 and all([0.7 < x < 0.9 for x in pos1]) and not all([0.7 < x < 0.9 for x in pos2]):
                agent.fitness += .1

            #collision
            if level >= 7:
                if all([abs(x1 - x2) < self.collision_dist for x1, x2 in zip(pos1, pos2)]) and not other_agent.collision:
                    agent.pos = [x2 - px2 + x1 for x2, x1, px2 in zip(pos2, pos1, other_agent.prev_pos)]
                    other_agent.pos = [x1 - px1 + x2 for x1, x2, px1 in zip(pos1, pos2, agent.prev_pos)]
                    agent.collision = True

        for agent in self.agents: agent.collision = False
        if self.drawer.enabled: self.drawer.draw()

    def generation_start(self, _):
        agents_by_specie = [self.agents_by_specie(i) for i in range(self.species_amount)]
        for agent in self.agents:
            self.food_pos[agent] = [random.uniform(0, 1) for _ in range(2)]
        '''
        self.relations = bidict({})
        for agent1, agent2 in zip(agents_by_specie[0], agents_by_specie[1]):
            self.relations[agent1] = agent2
            self.food_pos[agent1] = [random.uniform(0, 1) for _ in range(2)]
            self.food_pos[agent2] = self.food_pos[agent1]
            '''

    def generation_end(self, i):
        self.drawer.enabled = False

FlatEnv().run()
