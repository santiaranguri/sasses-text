import time
import pygame

class FlatDrawer:
    width = 1200
    height = 700

    white = (255, 255, 255)
    red = (255, 0, 0)
    green = (0, 255, 0)
    blue = (0, 0, 255)
    yellow = (0, 255, 255)
    black = (0, 0, 0)

    def __init__(self, env):
        self.env = env
        self.screen = pygame.display.set_mode((self.width, self.height))
        self.enabled = False
        pygame.font.init()
        self.font = pygame.font.SysFont('Arial', 12)

    def draw(self):
        self.screen.fill(self.black)
        agents = self.env.agents#[0:1]
        if self.env.level >= 3:
            other_agents = [self.env.get_enemy(agent) for agent in agents]
            agents + other_agents

        if self.env.level >= 4:
            square1 = self.map_to_screen([0.1, 0.1]) + self.map_to_screen([0.2, 0.2])
            pygame.draw.rect(self.screen, self.yellow, square1)

        if self.env.level >= 5:
            square2 = self.map_to_screen([0.7, 0.7]) + self.map_to_screen([0.2, 0.2])
            pygame.draw.rect(self.screen, self.blue, square2)

        for agent in agents:
            pos, specie, fitness, id = agent.pos, agent.specie, agent.fitness, agent.id
            color = self.green if specie == 0 else self.red
            pos = self.map_to_screen(pos)
            pygame.draw.circle(self.screen, color, pos, 10)
            text_surface = self.font.render('{}'.format(fitness), False, (self.white))
            self.screen.blit(text_surface, (pos[0] - 5, pos[1] - 5))

        for pos in [self.env.food_pos[agent] for agent in agents]:
            pygame.draw.circle(self.screen, self.blue, self.map_to_screen(pos), 3)

        pygame.display.flip()
        self.handle_click(agents[0])

    def handle_click(self, agent):
        clicked = True
        if clicked: time.sleep(.5)
        while not clicked:
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    clicked = True
                    food_pos = self.map_to_screen_inv([x for x in pygame.mouse.get_pos()])
                    self.env.food_pos[agent] = food_pos

    def map_to_screen(self, pos):
        return [int(x * screen * 0.95) + 15 for x, screen in zip(pos, [self.width, self.height])]

    def map_to_screen_inv(self, pos):
        return [(x - 15) / (screen * 0.95) for x, screen in zip(pos, [self.width, self.height])]
