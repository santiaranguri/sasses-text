import itertools
import numpy as np
#import tensorflow as tf
from mind.evolution import Evolution
from mind.flat_agent import FlatAgent

class Env:
    def __init__(self):
        self.agents_amount = 12
        self.species_amount = 2
        self.running_times = 50
        self.restoring_enabled = False
        self.optimizer = Evolution()
        self.exp_name = 'fixed_food_12'
        self.agents_per_specie = int(self.agents_amount / self.species_amount)
        self.parents_per_specie = int(self.agents_per_specie / 2)
        self.agents = [FlatAgent(j) for _ in range(self.agents_per_specie)
                       for j in range(self.species_amount)]
        self.acc_avg = [[] for _ in range(self.species_amount)]
        if self.restoring_enabled: self.restore()

    def run(self):
        #with tf.Session() as sess:
            #for agent in self.agents: agent.mind.define_vars(sess)
        for j in itertools.count():
            try:
                for agent in self.agents: agent.reset()
                self.generation_start(j)

                for i in range(self.running_times):
                    for agent in self.agents:
                        inpt = self.get_input(agent)
                        output = agent.mind.set_input(inpt)
                        self.set_output(agent, output)
                    self.iteration_end(i)

                self.generation_end(j)
                self.call_optimizer()
                self.print_progress(j)

            except KeyboardInterrupt:
                key = input()
                if key == 'd': self.drawer.enabled = True
                elif key == 'dd': self.drawer.enabled = False
                elif key == 's': self.save()
                elif key == 'i': self.interact()

    def call_optimizer(self):
        agents_by_specie = {i: self.agents_by_specie(i) for i in range(self.species_amount)}
        agents_to_kill, new_agents = self.optimizer.generation_end(agents_by_specie)
        for agent in agents_to_kill: self.agents.remove(agent)
        self.agents += new_agents

    def print_progress(self, j):
        print ('Generation {:04}.'.format(j), end=' ')

        for specie in range(self.species_amount):
            best_fitnesses = sorted(self.agents_by_specie(specie).values())
            avg = np.average(best_fitnesses[self.parents_per_specie:])
            self.acc_avg[specie].append(avg)
            acc_avg = np.average(self.acc_avg[specie][-250:])
            print ('Specie {}: Acc avg {:>6.2f}. Avg {:>6.2f}.'.format(specie, acc_avg, avg), end=' ')

        total_avg = [np.average(avg[-250:]) for avg in self.acc_avg]
        print ('Total avg {:>6.2f}'.format(np.average(total_avg)))

    def agents_by_specie(self, specie):
        return {agent: agent.fitness for agent in self.agents if agent.specie == specie}

    def save(self):
        for specie in range(self.species_amount):
            for i, agent in enumerate(self.agents_by_specie(specie)):
                path = 'exp_{}/specie{}/{}'.format(self.exp_name, specie, i)
                agent.mind.save(path)
        print ('saved')

    def restore(self):
        for specie in range(self.species_amount):
            for i, agent in enumerate(self.agents_by_specie(specie)):
                path = 'exp_{}/specie{}/{}'.format(self.exp_name, specie, i)
                agent.mind.restore(path)
        print ('restored')

    def interact(self):
        x = None #WIP
        print (self.agents[0].input(input()))
