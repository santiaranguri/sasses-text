import random
import numpy as np
from utils import sigmoid
import os

class NN:
    def __init__(self, structure, weights=None, biases=None):
        self.structure = structure
        random_weights = [np.random.normal(0, 0.1, (m, n)) for m, n in zip(self.sizes[1:], self.sizes[:-1])]
        random_biases = [np.random.normal(0, 0.1, n) for n in self.sizes[1:]]
        self.weights = random_weights if weights == None else weights
        self.biases = random_biases if biases == None else biases

    def set_input(self, x):
        for weight, bias in zip(self.weights, self.biases):
            x = sigmoid(np.dot(weight, x) + bias)
        return x

    def save(self, path):
        path = '../mind/data/{}'.format(path)
        os.makedirs(os.path.dirname(path), exist_ok=True)
        np.savez(path, sizes=self.sizes, weights=self.weights, biases=self.biases)

    def restore(self, path):
        npzfile = np.load('../mind/data/{}.npz'.format(path))
        self.sizes = npzfile['sizes']
        self.weights = npzfile['weights']
        self.biases = npzfile['biases']

    def mutate(self): # Pass this to evolution module
        for i, matrix in enumerate(self.weights):
            for j, column in enumerate(matrix):
                for k, _ in enumerate(column):
                    if random.randint(0, 1) == 0:
                        self.weights[i][j][k] += random.uniform(-.05, .05)
