import numpy as np
import tensorflow as tf
import time

class NN:
    def __init__(self, structure):
        self.structure = structure

    def define_vars(self, sess):
        self.sess = sess
        self.x = tf.placeholder(tf.float32, [None, self.size[0]])
        self.weights = [tf.Variable(tf.random_normal([m, n])) for m, n in zip(self.size, self.size[1:])]
        self.biases = [tf.Variable(tf.random_normal([m])) for m in self.size[1:]]

        x = self.x
        for w, b in zip(self.weights, self.biases):
            x = tf.add(tf.matmul(x, w), b)
            x = tf.nn.relu(x)

        self.output = x
        self.sess.run(tf.global_variables_initializer())

    def set_input(self, inpt):
        return self.sess.run(self.output, {self.x: [inpt]})[0]

    def mutate(self):
        new_weights = []
        for w, b in zip(self.weights, self.biases):
            m, n = [int(d) for d in np.shape(w)]
            x = np.random.randint(0, m, int(min(m, n) / 2))
            y = np.random.randint(0, n, int(min(m, n) / 2))
            indices = [[i, j] for i, j in zip(x, y)]
            updates = np.random.uniform(-.01, .01, len(indices))
            result = tf.scatter_nd_add(w, indices, updates)
            new_weights.append(result)
        self.weights = new_weights
        '''
        print(time.time())
            #biases = tf.unstack(biases)
            new_w = []
            for i, layer in enumerate(tf.unstack(w)):
                new_layer = []
                for j, value in enumerate(tf.unstack(layer)):
                    new_layer.append(value + np.random.uniform(-.01, .01))
                new_w.append(tf.stack(new_layer))
            new_weights.append(tf.stack(new_w))
        self.weights = new_weights
        print(time.time())
        '''

    def restore(self, path):
        pass
