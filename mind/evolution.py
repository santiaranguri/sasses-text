import operator
import copy
import numpy as np
from utils import normalize_vector

class Evolution:
    def generation_end(self, agents_by_specie):
        return self.asexual_reproduction(agents_by_specie)

    def sexual_reproduction(self, agents_by_specie):
        offspring_size = int(len(agents_by_specie[0]) / 2)
        worst_agents, new_agents = [[], []]
        for specie, agents in agents_by_specie.items():
            agents, fitnesses = zip(*agents.items())
            agents = [agent for agent in agents]
            fitnesses = normalize_vector(fitnesses)

            for _ in range(offspring_size):
                #new agents
                parent1, parent2 = [np.random.choice(agents, p=fitnesses) for _ in range(2)]
                child = copy.copy(parent1)
                child.mind.weights = self.merge_matrices(parent1.mind.weights, parent2.mind.weights)
                child.mind.biases = self.merge_matrices(parent1.mind.biases, parent2.mind.biases)
                child.mind.mutate()
                new_agents.append(child)

                #worst agents
                inv_fitnesses = np.ones(np.shape(fitnesses)) - fitnesses
                inv_fitnesses = normalize_vector(inv_fitnesses)
                agent = np.random.choice(agents, p=inv_fitnesses)
                agents.remove(agent)
                #TODO: remove fitness
                worst_agents.append(agent)

        return worst_agents, new_agents

    def asexual_reproduction(self, agents_by_specie):
        offspring_size = int(len(agents_by_specie[0]) / 2)
        worst_agents, new_agents = [[], []]

        for specie, agents in agents_by_specie.items():
            sorted_agents = sorted(agents.items(), key=operator.itemgetter(1))
            worst_agents = [agent for agent, _ in sorted_agents[:offspring_size]]
            best_agents = [agent for agent, _ in sorted_agents[offspring_size:]]

            new_agents = [copy.copy(agent) for agent in best_agents]
            for agent in new_agents: agent.mind.mutate()

        return worst_agents, new_agents

    @staticmethod
    def merge_matrices(matrix1, matrix2):
        shape = matrix1.shape# + matrix1[0].shape
        mask = np.random.randint(0, 1, shape)
        inv_mask = np.ones(shape) - mask
        return np.multiply(mask, matrix1) + np.multiply(inv_mask, matrix2)
