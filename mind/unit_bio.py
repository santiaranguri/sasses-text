from utils import sigmoid
import numpy as np

class Unit:
    def __init__(self, net, category):
        self.net = net
        self.category = category
        self.connections = []
        self.state = 0
        self.treshold = np.random.uniform(0, 1)

    def __lt__(self, other):
        return True

    def set_input(self, state):
        self.state += state
        if self.state > self.treshold: return self.activate()
        else: return []

    def activate(self):
        self.state = 0
        return [(conn['distance'], (conn['weight'], conn['to'])) for conn in self.connections]
