import random
import uuid
from mind.nn_bio import NN

class FlatAgent:
    def __init__(self, specie):
        self.id = ''
        self.specie = specie
        self.mind = NN({'sensor': 6, 'neuron': 64, 'actuator': 2})
        self.fitness = 0

    def reset(self):
        self.pos = [random.uniform(0, 1) for _ in range(2)]
        self.prev_pos = [0, 0]
        self.fitness = 0
        self.collision = False
