from heapq import heappush, heappop
import numpy as np
from mind.unit_bio import Unit

class NN:
    def __init__(self, structure):
        self.active_units = []
        self.time = 0
        self.units = [Unit(self, category) for category, times in structure.items() for _ in range(times)]
        self.init_connections()

    def init_connections(self):
        for unit1 in self.units:
            for unit2 in self.units:
                if np.random.randint(0, 2) == 0:
                    conn = {'to': unit2, 'weight': self.random_weight(), 'distance': np.random.uniform(0, 1)}
                    unit1.connections.append(conn)

    def set_input(self, data):
        for state, sensor in zip(data, self.by_category('sensor')):
            heappush(self.active_units, (0, (state, sensor)))

        for i in range(100):
            self.time += 1
            if len(self.active_units) == 0: break
            _, (state, unit) = heappop(self.active_units)
            operations = unit.set_input(state)

            for distance, (state, unit) in operations:
                heappush(self.active_units, (distance + self.time / 10000, (state, unit)))

        return [act.state for act in self.by_category('actuator')]

    def by_category(self, categories):
        return [unit for unit in self.units if unit.category in categories]

    def mutate(self):
        for unit in self.units:
            for i in range(len(unit.connections)):
                unit.connections[i]['weight'] += np.random.uniform(-.05, .05)
                unit.connections[i]['distance'] += np.random.uniform(-.05, .05)

    @staticmethod
    def random_weight():
        weight = np.random.uniform(0, 1)
        return weight if np.random.uniform(0, 1) > .8 else -weight
