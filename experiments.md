#Exp1
Agent initial pos.: fixed at [0, 0]
Food initial pos.: [random.uniform(0.2, 0.8) for _ in range(2)]
Input: self.agent_pos[agent] + self.food_pos

Behavior: the agents teleport to the position of the food
Fitness: {gen 1000: 1, Gen 7000: 70} (out of 100)
Rubook/index.php?md5=95FE91F171A107B0037A1FF48D3D9458ns: 2 out of 4 got a good fitness. Some cases got avg 100

#Exp2
Agent initial pos.: [random.uniform(0.2, 0.8) for _ in range(2)]
Food initial pos.: [random.uniform(0.2, 0.8) for _ in range(2)]
Input: self.agent_pos[agent] + self.food_pos

behavior: cyclic/espiral around a given point near the food (excellent :)

#Exp3
Whenever the food is eaten, it spawns in another location.

#Exp4
Every agent has its own food. And it starts in a random location. Whenever it's eaten, it spawns in another random location.

Result: it works. The agent (almost) teleports to the new location of the food.

#Exp5
Reduce the vel from exp4

Result: works like a charm (?

#Exp6
Now the food moves randomly

Result: works

#Exp7
Now the food is an agent

#Exp: complex map
##Exp: fixed food 1
min_dist = .1
velocity = 1
input = agent_pos + [0, 0] + food_pos
=> Total avg = 15
##Exp: fixed food 2
velocity = .5 => tavg = 11 (@500)
velocity = .2 => tavg = 5.5  (@500)
##Exp: fixed food 3
input = agent_pos + other_agent_pos + food_pos
tavg = 2.15 (@250)
tavg = 4    (@2000)
##Exp: fixed food 4
copy specie1 into specie0
Result: stagnated for the first 400 gens.
tavg = 6    (@5000)
##Exp: fixed food 5 (unsuccessful)
copy specie1 into specie0
tavg = 2    (@100)
tavg = 4    (@1000)
##exp: fixed food 6
added squares
(increase fitness given by squares by a factor of 5)
##exp: ff 7
if an agent touches the border, it loses 5 points
tavg = 7    (@1000)
##exp: ff 8
Added collisions


#Lesson 1
The more generations the better.
