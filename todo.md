#Roadmap
Finish implementing nn_tf
Implement crossover

#Main tasks
Pass nn to tensorflow
Don't make difference between species.
Fix collisions
Fix borders or think of other complexity.
Crossover
Uber papers
Test text-based and another mind module.
make the ai play agar.io?

#Other tasks
Automatically save whenever the total avg is bigger than (the best total avg saved + 1)
play with SD parameters
Compare trained agents to random agents to check whether they are getting better in the GAN thing.
+
#Text-based
a) Give it points whenever it says a valid word in English.
b) Use reddit
c) Manually give it points
d) Give it a Wikipedia article and ask for the gist
e) Give it lots of Wikipedia articles and ask the agent what article was the most related to a given input article
f) Teach it math

#Think
How can we let the agents create their own actuators/sensors?

#NN-bio
Make the distance between neurons realistic. Map the neurons in a map
